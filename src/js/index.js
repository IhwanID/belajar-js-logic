/*? no js js needed from me */

const board = document.querySelector(".board");

console.log(board);

const randomPosition = () => ~~(Math.random() * 15) + 1;

let config = {
  speed: 250,
  player: {
    x: randomPosition(),
    y: randomPosition(),
  },
  food: {
    x: randomPosition(),
    y: randomPosition(),
  },
  velocity: {
    x: 0,
    y: 0,
  },
  showTitle() {
    const title = document.getElementById("title__1");
    title.style.opacity = "1";
    title.style.visibility = "visible";
    title.style.zIndex = "1"; 

    setTimeout(function() {
      title.style.opacity = "0";
      title.style.visibility = "hidden";
      title.style.zIndex = "-1"; 
    }, 3000)
  }
};

const games = {
  createFood() {
    board.innerHTML = `<div class="food" style="grid-area: ${config.food.y} / ${config.food.x}"></div>`;
  },
  createPlayer() {
    board.innerHTML += `<div class="player" id="player" style="grid-area: ${config.player.y} / ${config.player.x}"></div>`;
  },
  movePlayer() {
    config.player.x += config.velocity.x;
    config.player.y += config.velocity.y;
  },
  resetPlayerPosition() {
    if (
      config.player.x <= 0 ||
      config.player.x > 15 ||
      config.player.y <= 0 ||
      config.player.y > 15
    ) {
      console.log("aw kalah....");
      config.player.x = 7;
      config.player.y = 7;
    }
  },
  randomFoodPosition() {
    config.food.x = randomPosition();
    config.food.y = randomPosition();
  },
  isWin() {
    if (config.player.x === config.food.x && config.player.y === config.food.y){
        return true
    } 
    return false
  },
};

function movement(listen) {
  switch (listen.key) {
    case "ArrowUp":
    case "w":
      config.velocity.y = -1;
      config.velocity.x = 0;
      break;
    case "ArrowDown":
    case "s":
      config.velocity.y = 1;
      config.velocity.x = 0;
      break;
    case "ArrowRight":
    case "d":
      config.velocity.y = 0;
      config.velocity.x = 1;
      break;
    case "ArrowLeft":
    case "a":
      config.velocity.y = 0;
      config.velocity.x = -1;
      break;
    default:
      break;
  }
}

function headMovement() {
    const playerEl = document.getElementById("player")
    playerEl.style.transform = "scale(1)" 
    if(config.velocity.x == 1) {
        playerEl.style.transform = "scaleX(-1)"
      }
      if(config.velocity.y == -1) {
        playerEl.style.transform = "rotate(90deg)"
      }
      if(config.velocity.y == 1) {
        playerEl.style.transform = "rotate(-90deg)"
      }
}

function start() {
  games.createFood();
  games.createPlayer();
  games.movePlayer();
  headMovement();
  games.resetPlayerPosition();

  const isWin = games.isWin();
  if (isWin) games.randomFoodPosition();
}

setInterval(start, config.speed);
document.addEventListener("keydown", movement);
